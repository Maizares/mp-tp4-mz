import os
def presionarTecla():
    u = input('presione una tecla para continuar')

def menu():
    os.system('cls')
    print('1)  Registrar Productos')
    print('2)  Mostrar el listado de productos')
    print('3)  Mostrar los productos cuyo stock se encuentren en el intervalo [desde, hasta]')
    print('4)  Sumar X al sotck de todos los productos cuyo valor actual de stock sea menor a Y')
    print('5)  Eliminar todos los productos cuyo stock sea igual a cero ')
    print('6)  Salir')
    eleccion = int(input('Elija una opcion: '))
    while eleccion > 6 or eleccion < 1 :
        eleccion = int(input('Elija una opcion: '))
    return eleccion 

def eliminarProducto(diccionario):
    os.system('cls')
    print('Se eliminaran todos los productos cuyos Stock = 0')
    i = 1
    while i <= len(diccionario):
        for clave, valor in diccionario.items():
            if (valor[2] == 0):
                print('se va a eliminar: ', diccionario[clave])
                del diccionario[clave]
                print('Eliminado exitosamente')
                break
        i+=1
    return (diccionario)

def procesoSumaX(diccionario):
    os.system('cls')
    y = int(input('Ingrese un valor: '))
    x = int(input('Ingrese el valor que incrementara a todos los stocks: '))
    for clave, valor in diccionario.items():
        if (valor[2] < y):
            valor[2] = valor[2] + x
    print('valores aumentados exitosamente')

def mostrarDesdeHasta(diccionario):
    os.system('cls')
    desde = int(input('Desde stock: '))
    hasta = int(input('Hasta stock: '))
    for clave, valor in diccionario.items(): 
        if (valor[2] >= desde) and (valor[2] <=  hasta):
            print(clave,valor)

def mostrarProductos(diccionario):
    os.system('cls')
    print('*** LISTADO DE PRODUCTOS ***')
    for clave, valor in diccionario.items():
        print(clave,valor)

def validarPrecio():
    x = input('Precio: ')
    while not (x != ''):
        x = input('Precio: ')
    x = float(x)
    while not (x > -1):
        x = float(input('Precio: '))
    return x

def validarStock():
    x = input('Stock: ')
    while not (x != ''):
        x = input('Stock: ')
    x = int(x)
    while not (x > -1):
        x = int(input('Stock: '))
    return x

def validarCodigo():
    x = input('\nCodigo: ')
    while not (x != ''):
        x = input('Codigo: ')
    x = int(x)
    return x

def validarDescripcion():
    x = input('Descripcion: ')
    while not (x != ''):
        x = input('Descripcion: ')
    return x

def CargarProductos():
    productos = {}
    print('***CARGAR PRODUCTOS***')
    res = 's'
    while not (res =='n') or (res == 'N'):
        codigo = validarCodigo()
        if codigo not in productos :
            descripcion = validarDescripcion()
            precio = validarPrecio()
            stock = validarStock()
            productos[codigo] = [descripcion, precio, stock]
            print ('\nProducto cargado correctamente')
            res= input('Desea cargar otro producto? s/n : ')
        else : 
            print ('El Producto ya existe')
    return productos


#PRINCIPAL
opcion = 0
productos={}
while opcion != 6:
    opcion = menu()
    if opcion == 1:
        os.system('cls')
        productos = CargarProductos()
        presionarTecla()
    elif opcion == 2:
        os.system('cls')
        mostrarProductos(productos)
        presionarTecla()
    elif opcion == 3:
        os.system('cls')
        mostrarDesdeHasta(productos)
        presionarTecla()
    elif opcion == 4:
        os.system('cls')
        procesoSumaX(productos)
        presionarTecla()
    elif opcion == 5:
        os.system('cls')
        eliminarProducto(productos)
        presionarTecla()



